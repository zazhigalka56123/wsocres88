package com.wscores.matchs.reslts.sport.second.api

import android.content.Context
import android.webkit.WebSettings
import com.wscores.matchs.reslts.sport.UTIL.jhjmgj
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class ApiBuilder {
    fun service(context: Context, url: String): ApiService {
        val client: OkHttpClient = OkHttpClient.Builder().addInterceptor { chain ->
            val newRequest: Request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("x-rapidapi-key", jhjmgj)
                .addHeader("User-Agent", WebSettings.getDefaultUserAgent(context).toString())
                .build()
            chain.proceed(newRequest)
        }.build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create()
    }

}