package com.wscores.matchs.reslts.sport.first

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.backendless.Backendless
import com.backendless.async.callback.AsyncCallback
import com.backendless.exceptions.BackendlessFault
import com.backendless.persistence.DataQueryBuilder
import com.wscores.matchs.reslts.sport.R
import com.wscores.matchs.reslts.sport.UTIL
import com.wscores.matchs.reslts.sport.UTIL.DGDG
import com.wscores.matchs.reslts.sport.UTIL.fdsfdsf
import com.wscores.matchs.reslts.sport.databinding.PhoneBinding
import com.wscores.matchs.reslts.sport.second.ui.ScreenAct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.FormatWatcher
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


class FragmentPhone: Fragment() {
    val queryBuilder = DataQueryBuilder.create().also { it.setPageSize(100) }
    var num = ""
    var save = true
    var finish = false
    var isError = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = PhoneBinding.inflate(layoutInflater, container, false)

        val slots = UnderscoreDigitSlotsParser().parseSlots("+7 (9__) ___-__-__")

        val mask = MaskImpl.createTerminated(slots)
        mask.isForbidInputWhenFilled = true;

        val formatWatcher: FormatWatcher = MaskFormatWatcher(mask)
        formatWatcher.installOnAndFill(v.terwerwe)

        val ss = SpannableString(requireContext().getString(R.string.ewefsdfsd))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                findNavController().navigate(R.id.fsdfds)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }
        }
        ss.setSpan(clickableSpan, 59, 87, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        val clickableSpan2: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                findNavController().navigate(R.id.fdsfdsffdsfsd1)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }
        }
        ss.setSpan(clickableSpan2, 26, 45, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        v.licdscsd.text = ss
        v.licdscsd.movementMethod = LinkMovementMethod.getInstance()
        v.licdscsd.highlightColor = Color.TRANSPARENT

        v.bdsdbsd.setOnClickListener {
            it.isClickable = false
            it.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.fsdfsdfsd))
            var phone = "+"
            v.terwerwe.text.toString().forEach {
                if (it.isDigit())
                    phone += it
            }
            if (v.ch4adsda.isChecked){
                if (phone.length == 12){
                    num = phone
                    save = true
                    finish = false
                    lifecycleScope.launch(Dispatchers.IO) {
                        ddgfgdgfdgffd()
                        while (true) {
                            if (finish) {
                                finish = false
                                if (isError) {
                                    isError = false
                                    requireActivity().runOnUiThread {
                                        Toast.makeText(
                                            requireContext(),
                                            requireContext().getString(R.string.gddggdg),
                                            Toast.LENGTH_LONG
                                        ).show()
                                        it.isClickable = true
                                        it.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.dsadasasdas))
                                    }
                                } else {
                                    val sp = requireActivity().getSharedPreferences(
                                        UTIL.SP,
                                        Context.MODE_PRIVATE
                                    )
                                    sp.edit {
                                        putBoolean(UTIL.key, false)
                                        commit()
                                    }
                                    requireActivity().runOnUiThread {
                                        startActivity(Intent(requireContext(), ScreenAct::class.java))
                                        requireActivity().finish()
                                    }
                                }
                            }else{
                                delay(100)
                            }
                        }
                    }
                }else{
                    it.isClickable = true
                    it.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.dsadasasdas))
                    Toast.makeText(requireContext(), requireContext().getString(R.string.fsdfdsfsdfsd), Toast.LENGTH_LONG).show()
                }
            }else{
                it.isClickable = true
                it.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.dsadasasdas))
                Toast.makeText(requireContext(), requireContext().getString(R.string.rweewrerwgd), Toast.LENGTH_LONG).show()

            }
        }

        return v.root
    }


    private fun ddgfgdgfdgffd() {
        Backendless.Data.of(DGDG).find(queryBuilder, object : AsyncCallback<List<Map<*, *>?>?> {
            override fun handleResponse(foundContacts: List<Map<*, *>?>?) {
                if (foundContacts?.size != 0) {
                    foundContacts?.forEach {

                        if(it?.containsKey(fdsfdsf) == true && it[fdsfdsf].toString() == num){
                            save = false
                        }
                    }
                    queryBuilder.prepareNextPage()
                    ddgfgdgfdgffd()

                }else{
                    if(save){
                        lifecycleScope.launch(Dispatchers.IO) {
                            Backendless.Data.of(DGDG).save(mapOf(fdsfdsf to num))
                            finish = true
                        }
                    }else{
                        finish = true

                    }
                }
            }

            override fun handleFault(fault: BackendlessFault) {
                finish = true
                isError = true
            }
        })
    }

}