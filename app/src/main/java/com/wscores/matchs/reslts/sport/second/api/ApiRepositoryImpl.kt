package com.wscores.matchs.reslts.sport.second.api

import android.content.Context

import com.wscores.matchs.reslts.sport.second.models.M
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.text.SimpleDateFormat
import java.util.Calendar

class ApiRepositoryImpl(context: Context, url: String) : ApiRepository {


    private val service = ApiBuilder().service(context, url)


    override suspend fun getBasss(date: String, league: Int, season: String, timezone: String): Flow<List<M>> = flow {
        with(service.getBasket(date, league, season, timezone)) {
            val list = mutableListOf<M>()
            if (isSuccessful){
                if ((body()?.results ?: 0) > 0) {
                    body()?.response?.forEach { response ->
                        with(response) {
                            val el = M(
                                team1 = fdsfdsfsd?.home?.name.toString(),
                                team2 = fdsfdsfsd?.away?.name.toString(),
                                time = time.toString(),
                                logo1 = fdsfdsfsd?.home?.logo.toString(),
                                logo2 = fdsfdsfsd?.away?.logo.toString(),
                                score1 = scores?.home?.total?.toString()?.toInt(),
                                score2 = scores?.gdgdggd?.total?.toString()?.toInt(),
                                status = etwerewrew?.long.toString(),
                                league = this.league?.name.toString()

                            )
                            list.add(el)
                        }
                    }
                }
            }
            emit(list.toList())
        }
    }.catch {
        emit(emptyList())
    }
        .flowOn(Dispatchers.IO)

    override suspend fun getFoott(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<M>> = flow {
        with(service.getFoot(date,
            season, timezone)) {
            val list = mutableListOf<M>()
            if (isSuccessful){
                if ((body()?.results ?: 0) > 0) {
                    body()?.response?.forEach { response ->
                        with(response) {
                            val el = M(
                                team1 = teams?.home?.name.toString(),
                                team2 = teams?.away?.name.toString(),
                                time = getDfdsfsdfs(fixture?.date.toString()),
                                logo1 = teams?.home?.logo.toString(),
                                logo2 = teams?.away?.logo.toString(),
                                score1 = goals?.home?.toInt(),
                                score2 = goals?.away?.toInt(),
                                status = fixture?.status?.long.toString(),
                                league = this.league?.name.toString()

                            )
                            list.add(el)
                        }
                    }
                }
            }
            emit(list.toList())
        }
    }.catch {
        emit(emptyList())
    }.flowOn(Dispatchers.IO)

    override suspend fun getHHHfsdfds(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ):  Flow<List<M>> = flow {
        with(service.getHHHHHfsdfsdf(date, league, season, timezone)) {
            val list = mutableListOf<M>()
            if (isSuccessful){
                if ((body()?.results ?: 0) > 0) {
                    body()?.response?.forEach { response ->
                        with(response) {
                            val el = M(
                                team1 = teams?.home?.name.toString(),
                                team2 = teams?.away?.name.toString(),
                                time  =  time.toString(),
                                logo1 = teams?.home?.logo.toString(),
                                logo2 = teams?.away?.logo.toString(),
                                score1 = scores?.home,
                                score2 = scores?.away,
                                status = status?.long.toString(),
                                league = this.league?.name.toString()

                            )
                            list.add(el)
                        }
                    }
                }
            }
            emit(list.toList())
        }
    }.catch {
        emit(emptyList())
    }.flowOn(Dispatchers.IO)

    override suspend fun getVdfsfsd(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<M>> = flow {
        with(service.getVSfdsfsd(date,
            timezone)) {
            val list = mutableListOf<M>()
            if (isSuccessful){
                if ((body()?.results ?: 0) > 0) {
                    body()?.response?.forEach { response ->
                        with(response) {
                            val el = M(
                                team1 = teams?.home?.name.toString(),
                                team2 = teams?.away?.name.toString(),
                                time  =  time.toString(),
                                logo1 = teams?.home?.logo.toString(),
                                logo2 = teams?.away?.logo.toString(),
                                score1 = scores?.home,
                                score2 = scores?.away,
                                status = status?.long.toString(),
                                league = this.league?.name.toString()

                            )
                            list.add(el)
                        }
                    }
                }
            }
            emit(list.toList())
        }
    }.catch {
        emit(emptyList())
    }.flowOn(Dispatchers.IO)

    private fun getDfdsfsdfs(str: String): String {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val d = format.parse(str.substring(0, 18))
        val c = Calendar.getInstance()
        c.time = d
        var h = c.get(Calendar.HOUR_OF_DAY).toString()
        if (h.length == 1){
            h = "0$h"
        }
        var t = c.get(Calendar.MINUTE).toString()
        if (t.length == 1){
            t = "0$t"
        }
        return "$h:$t"

    }
}