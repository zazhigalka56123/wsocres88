package com.wscores.matchs.reslts.sport.second.recyclerview

import androidx.recyclerview.widget.RecyclerView
import com.wscores.matchs.reslts.sport.databinding.ItemBinding


class MHolder(val binding: ItemBinding) : RecyclerView.ViewHolder(binding.root)
